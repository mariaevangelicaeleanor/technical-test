import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import listData from './component/listData';
import HeaderComponent from './component/HeaderComponent';
import FooterComponent from './component/FooterComponent';
import addData from './component/addData';
import getData from './component/getData';

function App() {
  return (
    <div>
        <Router>
              <HeaderComponent/>
                <div className="container">
                    <Switch> 
                          <Route path = "/" exact component = {listData}></Route>
                          <Route path = "/data" component = {listData}></Route>
                          <Route path = "/addData/:id" component = {addData}></Route>
                          <Route path = "/getData/:id" component = {getData}></Route>
                    </Switch>
                </div>
                <br></br>
              <FooterComponent/>
        </Router>
    </div>
    
  );
}

export default App;