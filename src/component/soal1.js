// React is loaded and is available as React and ReactDOM
// imports should NOT be used
const TodoItem = (props) => <li onClick={props.onClick}>{props.item.text}</li>
class TodoList extends React.Component {
 render() {
 const { items, onListClick } = this.props;
 return (<ul onClick={onListClick}>
 {items.map((item, index) => 
 <TodoItem item={item} key={index} onClick={this.handleItemClick.bind(this, item)}/>)}
 </ul>);
 }
 
 handleItemClick(item, event) {
 // Write your code here
 //check for item as first parameters if that false than called 
 //onItemClick with item and event same as App. props for passing data 
  if(item.done == false){
    this.props.onItemClickk(item,even)
  }
 //based on question explanation when done == true than  stop propagation 
  else{
    event.stopPropagation()
  }
 }
}

const items = [ { text: 'Buy grocery', done: true },
{ text: 'Play guitar', done: false },
 { text: 'Romantic dinner', done: false }
];
const App = (props) => <TodoList
 items={props.items}
 onListClick={(event) => console.log("List clicked!")}
 onItemClick={(item, event) => { console.log(item, event) }}
/>;
document.body.innerHTML = "<div id='root'></div>";
const rootElement = document.getElementById("root");
ReactDOM.render(<App items={items}/>, rootElement);
